<cfcomponent name="forms" output="no">
	<!--- form fieldsets. each fieldset must have at least one associated form field. --->
	<cfset this.groups = ArrayNew(1) />
	
	<!--- form fields. They can be within a fieldset or not. --->
	<cfset this.fields = ArrayNew(1) />
	
	<!--- GRID of form fields. They can not be within a fieldset right now. --->
	<cfset this.grids = ArrayNew(1) />
	
	<!-- form name, used in email --->
	<cfset this.formname = "" />
	
	<!-- form id, used in HTML form tag --->
	<cfset this.formid = "" />
	
	<!-- form class, used in HTML form tag --->
	<cfset this.formclass = "" />
	
	<!--- name of Javascript function used for form validation --->
	<cfset this.formhandler = "checkRequired" />
	
	<!-- site logo, used in email --->
	<cfset this.logo = "" />
	
	<!--- spam trap field - if form.formFirstname is not empty, then it's spam. --->
	<cfset this.spamfield = "formFirstname" />
	
	<!-- datasource that we save the data to --->
	<cfset this.dsn = "" />
	
	<!-- database table that we save the data to --->
	<cfset this.table = "" />
	
	<!---
		Required table fields:
		id VARCHAR(35)
		dateentry DATETIME
	--->
	
	
	<cffunction name="start">
		<cfargument name="formname" required="true" />
		<cfargument name="formid" required="false" default="contactus" type="string" />
		<cfargument name="formclass" required="false" default="simple" type="string" />
		<cfargument name="dsn" required="true" type="string" />
		<cfargument name="table" required="true" type="string" />
		<cfargument name="formhandler" required="true" default="checkRequired" type="string" />
		
		<cfset this.formname = arguments.formname />
		<cfset this.formid = arguments.formid />
		<cfset this.formclass = arguments.formclass />
		<cfset this.dsn = arguments.dsn />
		<cfset this.table = arguments.table />
		<cfset this.formhandler = arguments.formhandler />
		
		<cfreturn this />
	</cffunction>
	
	<cffunction name="newGroup" hint="Add a new input to the form.">
		<cfargument name="name" required="true" type="string" />
		<cfargument name="label" required="true" type="string" />
		<cfargument name="fields" required="false" default="text" type="string" />
		
		<cfset var tempGroup = {
			name	= arguments.name,
			label	 = arguments.label,
			fields	= arguments.fields,
		} />
		
		<cfset ArrayAppend(this.groups,tempGroup) />
		
		<cfreturn newGroup />
	</cffunction>
	<cffunction name="newInput" hint="Add a new input to the form.">
		<cfargument name="name" required="true" type="string" />
		<cfargument name="label" required="true" type="string" />
		<cfargument name="type" required="false" default="text" type="string" />
		<cfargument name="required" required="false" default="false" type="boolean" />
		<cfargument name="enabled" required="false" default="true" type="boolean" />
		<cfargument name="value" required="false" default="" type="string" />
		<cfargument name="validatetype" required="false" default="" type="string" />
		
		<cfset var tempInp = {
			name		= arguments.name,
			label 		= arguments.label,
			type		= arguments.type,
			required	= arguments.required,
			enabled 	= arguments.enabled,
			value 		= arguments.value,
			validatetype=arguments.validatetype
		} />
		
		<cfset ArrayAppend(this.fields,tempInp) />
		
		<cfreturn tempInp />
	</cffunction>
	<cffunction name="newGrid">
		<cfargument name="wrapclass" required="true" hint="The class on the div wrapping the form field(s)" />
		<cfargument name="fieldclass" required="true" hint="The class on each form field's block div." />
		<cfargument name="fields" required="true" hint="The list of fields that go in this grid div." />
		
		<cfset var tempGrid = {
			wrapclass 	= arguments.wrapclass,
			fieldclass	= arguments.fieldclass,
			fields		= arguments.fields
		} />
		
		<cfset ArrayAppend(this.grids,tempGrid) />
		
		<cfreturn tempGrid />
	</cffunction>
	<cffunction name="checkSpam" hint="See if form submission is spam. If it is spam, returns false" output="yes" returnType="struct">
		<cfargument name="printerr" type="boolean" default="true" />

		<cfset var retval = {
			err = "",
			valid = true
		} />
		
		<cfif NOT StructKeyExists(form,spamtrap)>
			<cfset retval.err = "Form spamtrap field could not be found. Are you spam?" />
			<cfset retval.valid = false />
		<cfelse>
			<cfif NOT StructKeyExists(form,form.spamtrap)>
				<cfset retval.err = "Spamtrap specified by the form spamtrap field could not be found. Are you spam? form[spamtrap]:#form.spamtrap#" />
				<cfset retval.valid = false />
			<cfelse>
				<cfif form[form.spamtrap] NEQ "">
					<cfset retval.err = "Spamtrap field is not empty, are you spam?" />
					<cfset retval.valid = false />
				</cfif>
			</cfif>
		</cfif>
		
		<cfif printerr AND retval.err NEQ "">
			<cfoutput><p class="alert alert-danger">#retval.err#</p></cfoutput>
		</cfif>
		
		<!--- return true/false OR a string error message --->
		<cfreturn retval />
	</cffunction>
	<cffunction name="validate" hint="Check the form scope to see if all required values were submitted." returnType="struct" output="yes">
		<cfargument name="printerr" type="boolean" default="true" />
		
		<!--- use the form scope. --->
		<cfset var retval = {
			valid = true
		} />
		
		<!--- loop through each required field, see if it's included in the form fields and if it has a value. --->
		<cfloop array="#this.fields#" index="field">
			<cfif NOT field.required><cfcontinue /></cfif><!--- only worry about required fields. --->
			
			<cfif Not StructKeyExists(form,"field_" & field.name) OR form["field_" & field.name] EQ "">
				<cfif printerr>
					<p class="alert alert-danger">#field.name# is required.</p>
				</cfif>
				<cfset retval.valid = false />
			</cfif>
		</cfloop>
		
		<!--- return true/false AND an array of errors, eventually --->
		<cfreturn retval />
	</cffunction>
	<cffunction name="saveToDatabase" hint="Insert the new data, return struct with success(bool) and newid(string) values." output="no" returnType="struct">
		
		<cfset var newID = CreateUUID() />
		<cfset var i = 0 />
		
		<!--- save to database --->
		<cfquery name="savetodb" dataSource="#this.dsn#">
			INSERT INTO #this.table# (
				id,
				entrydate<cfif ArrayLen(this.fields) GT 0>,</cfif>
				<cfloop array="#this.fields#" index="field">
					<cfset i++ />
					#field.name#<cfif i NEQ ArrayLen(this.fields)>,</cfif>
				</cfloop>
			)
			VALUES (
			<cfset var i = 0 />
				<cfqueryparam value="#newID#" />,
				<cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp" /><cfif ArrayLen(this.fields) GT 0>,</cfif>
				<cfloop array="#this.fields#" index="field">
					<cfset i++ />
					<cfif StructKeyExists(form,"field_" & field.name)>
						<cfqueryparam value="#form['field_#field.name#']#" /><cfif i NEQ ArrayLen(this.fields)>,</cfif>
					</cfif>
				</cfloop>
			)
		</cfquery>
		
		<!--- return struct with boolean AND id of new item. --->
		<cfset var retval = {
			success = "true",
			newid = newID,
			msg = ""
		} />
		
		<!---
			TODO: if there's any error we set retval.msg = "error details..."
	--->
		
		<cfreturn retval />
	</cffunction>
	<cffunction name="sendNotificationEmail" output="no">
		<cfargument name="to" required="true" />
		
		<cfmail from="#arguments.from#" to="#arguments.from#" replyto="#arguments.from#" subject="Form submission on #this.formname#" TYPE="html">
			<cfset this.renderEmail() />
		</cfmail>
	</cffunction>
	<cffunction name="renderInputsNotInGroup">
		
		<cfset this.renderFormGrids() />
<!---
		
		<!--- Call renderInputs with blank group argument --->
		<cfset this.renderInputs('') />
--->
		
			<cfreturn true />
	</cffunction>
	<cffunction name="renderformGroups" output="yes" returnType="boolean">
		
		<cfif NOT ArrayIsEmpty(this.groups)>
			<!--- then loop by form groups, leaving non-associated form fields for the end. --->
			<cfloop array="#this.groups#" index="group">
				<cfoutput>
					<fieldset id="form_group_#group.name#">
						<legend>#group.label#</legend>
						<cfset this.renderInputs(group.fields) />
					</fieldset>
				</cfoutput>
			</cfloop>
		</cfif>
		
		<cfreturn true />
	</cffunction>
	<cffunction name="renderFormGrids">
		
		<cfif NOT ArrayIsEmpty(this.grids)>
			<!--- then loop by form groups, leaving non-associated form fields for the end. --->
			<cfloop array="#this.grids#" index="grid">
				<cfoutput>
					<div class="#grid.wrapclass#">
						<cfloop list="#grid.fields#" index="field">
						<!--- make a grid column --->
						<div class="#grid.fieldclass#">
							<!--- render ONLY this input inside this particular grid column. --->
							<cfset this.renderInputs(field) />
						</div>
						</cfloop>
					</div>
				</cfoutput>
			</cfloop>
		</cfif>
		
		<cfreturn true />
	</cffunction>
	<cffunction name="safeLabel">
		<cfargument name="label" required="true" />
		
		<cfreturn REreplaceNoCase(arguments.label,"[^a-zA-Z0-9_-]", "", "ALL") />
	</cffunction>
	<cffunction name="renderInput" output="yes" returnType="boolean">
		<cfargument name="field" required="true" />
		
		<cfset var id = "field_#this.safeLabel(arguments.field.name & arguments.field.value)#" />
		<cfset var classes = "" />
		
		<cfif field.required>
			<cfset classes &= " req" />
		</cfif>
		<cfif field.validatetype NEQ "">
			<cfset classes &= " validate #arguments.field.validatetype#" />
		</cfif>
		
		<cfoutput>
		<cfswitch expression="#arguments.field.type#">
			<cfcase value="textarea">
				<textarea name="field_#arguments.field.name#" id="#id#" class="input #classes#"></textarea>
			</cfcase>
			<cfdefaultcase>
				<input name="field_#arguments.field.name#" type="#arguments.field.type#" id="#id#" class="input #classes#" value="#arguments.field.value#">
			</cfdefaultcase>
		</cfswitch>
		</cfoutput>
		
		<cfreturn true />
	</cffunction>
	<cffunction name="renderLabel" output="yes" returnType="boolean">
		<cfargument name="field" required="true" />
		
		<cfset var id = "field_#this.safeLabel(arguments.field.name & arguments.field.value)#" />
		<cfoutput><label for="#id#" class="title">#arguments.field.label#:</label></cfoutput>
		
		<cfreturn true />
	</cffunction>
	<cffunction name="renderReqLabel" output="no" returnType="string">
		<cfargument name="field" required="true" />
		
		<cfif arguments.field.required>
			<cfoutput><span class="required"><img src="http://cms.tspark.com/tsparkcms/images/icon_required.png" alt="This field is required" /></span></cfoutput>
		</cfif>
		
		<cfreturn true />
	</cffunction>
	<cffunction name="renderInputs" output="yes" returnType="boolean">
		<cfargument name="ingroup" type="string" required="false" hint="Specify that we only show the inputs in group X" />
		
		<cfloop array="#this.fields#" index="field">
			<cfif arguments.ingroup GT "">
				<cfif NOT ListFind(arguments.ingroup,field.name)>
					<!--- if not in specified group, skip this field. --->
					<cfcontinue />
				</cfif>
			</cfif>
			<!---<cfdump var="#field#" />--->
			<cfif field.enabled>
				<cfoutput><div class="block fm-line<cfif field.required> fm-req</cfif><cfif field.type EQ "radio"> fm-radio</cfif><cfif field.type EQ "checkbox"> fm-checkbox</cfif>">
				<cfswitch expression="#field.type#">
					<cfcase value="radio,checkbox">
						<cfset this.renderInput(field) />
						<cfset this.renderLabel(field) />
						<cfset this.renderReqLabel(field) />
					</cfcase>
					<cfdefaultcase>
						<cfset this.renderLabel(field) />
						<cfset this.renderReqLabel(field) />
						<cfset this.renderInput(field) />
					</cfdefaultcase>
				</cfswitch>
				
				<div class="fm-help"></div>
			</div><!--//.block--></cfoutput>
			</cfif>
		</cfloop>
		
		<cfreturn true />
	</cffunction>
	<cffunction name="renderForm" hint="Outputs the HTML form, complete with JS validation" output="yes">
		<cfoutput>
		<form id="form_#this.formid#" action="?" method="post" class="#this.formclass#" onsubmit="return #this.formhandler#($(this))">
			<input type="hidden" name="action" value="submit" />
			<input type="hidden" name="spamtrap" value="#this.spamfield#" />
			<div class="formFirstname">
				<label for="formFirstname">Spam trap - do not use!</label>
				<input name="#this.spamfield#" type="text" id="formFirstname" class="formFirstname" />
			</div>
			
			<div class="fm-fields">
				<!--- show inputs which are in a fieldset group --->
				<cfset this.renderformGroups() />
				
				<!--- show inputs which aren't in a group --->
				<cfset this.renderInputsNotInGroup() />
				
				<div class="block fm-line fm-submit">
					<input name="submit" type="submit" value="Submit" id="submit" class="submit blueButton"	/>
				</div>
			</div><!--//.fm-fields-->
			
			<div class="fm-alerts"></div>
		</form>
		</cfoutput>
		
	</cffunction>
	<cffunction name="renderEmail" hint="Contains the HTML email template for the notification message" output="yes">
		
		<cfoutput>
			<html>
			<head>
			</head>
			<body>
				<cfif this.logo NEQ "">
					<p style="text-align: left;padding-bottom: 5px;padding-top:15px;margin-top:0;"><img src="#this.logo#" alt="#this.formname#" /></p>
				</cfif>
				<h1 style="text-align: left;font-size:22px;font-family:Arial,sans-serif;color:##000;padding:.5em 0;line-height:1.3em;margin:0;">#this.formname#</h1>
		
				<p style="text-align: left;font-size:18px;font-family:Arial,sans-serif;color:##000;padding:.5em 0;line-height:1.3em;margin:0;">
					<cfloop array="#this.fields#" index="field">
					<strong>#field.label#:</strong> #field.name#<br />
					</cfloop>
				</p>
				<p style="text-align: left;font-size:12px;font-family:Arial,sans-serif;color:##000;padding:.5em 0;line-height:1.3em;">
				----------------------------------------------------------------<br />
				Date Submitted: #DateFormat(Now (), "mm/dd/yyyy")#<br />
				Time Submitted: #TimeFormat(Now (), "h:MM TT")#<br />
				IP Address: #cgi.REMOTE_ADDR#<br />
				User Agent: #cgi.HTTP_USER_AGENT#<br />
				Form URL: #cgi.HTTP_Referer#</p>
				
			</body>
			</html>
		</cfoutput>
		
	</cffunction>
</cfcomponent>
