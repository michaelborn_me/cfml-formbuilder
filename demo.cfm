<cfparam name="form.action" default="">

<!--- Start timing test --->
<cfset tickBegin = GetTickCount()>

<!---
  Table structure

CREATE TABLE `donnmartmp`.`returns` (
  `id` VARCHAR(35) NOT NULL,
  `entrydate` DATETIME NULL,
  `name` VARCHAR(155) NULL,
  `return_address` VARCHAR(155) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `model` VARCHAR(155) NULL,
  `description` TEXT NULL,
  `leather_sheath` VARCHAR(1) NULL,
  `cordura_sheath` VARCHAR(1) NULL,
  `tether` VARCHAR(1) NULL,
  PRIMARY KEY (`id`));
--->


<cfset formObj = CreateObject("src.formobj").start(
  formname="Warranty Form",
  formid="warranty_return",
  dsn="donnmar",
  table="returns"
) />

<cfset formObj.newInput(
  name = "name",
  label="Name",
  type="text",
  required="true"
) />
<cfset formObj.newInput(
  name = "return_address",
  label="Return Address",
  type="text",
  required="true"
) />
<cfset formObj.newInput(
  name = "phone",
  label="Phone",
  type="text",
  required="true",
  validatetype="phone"
) />
<cfset formObj.newInput(
  name = "email",
  label="Email",
  type="email",
  required="true",
  validatetype="email"
) />
<cfset formObj.newInput(
  name = "model",
  label="Model",
  type="text",
  required="true"
) />
<cfset formObj.newInput(
  name = "description",
  label="Description of problem / Service to be done / Comments",
  type="textarea",
  required="true"
) />

<!---
	group of radio buttons
--->
<cfset formObj.newGroup(
	name="sheath_type",
	label="What kind of sheath would you like?",
	type="radio",
	fields="sheath_type"
) />
<cfset formObj.newInput(
  name = "sheath_type",
  label="Leather",
  type="radio",
  value="leather",
  required="true"
) />
<cfset formObj.newInput(
  name = "sheath_type",
  label="Cordura",
  type="radio",
  value="cordura",
  required="true"
) />
<cfset formObj.newInput(
  name = "sheath_type",
  label="None",
  type="radio",
  value="none",
  checked="true",
  required="true"
) />

<!--- add the custom fieldsets --->
<cfset formObj.newGroup(
  name="return_addr",
  label="Return Address",
  fields="name,return_address,phone,email"
) />
<cfset formObj.newGroup(
  name="pliers",
  label="Pliers",
  fields="model,description"
) />


<cfswitch expression="#form.action#">
  <cfcase value="submit">
    <cfset formIsValid = formObj.validate() />
    <cfif NOT formIsValid><!--- do not allow invalid form to continue. --->
      <cfexit />
    </cfif>
    
    <cfset userIsValid = formObj.checkSpam() />
    <cfif NOT userIsValid><!--- do not allow spam bot to continue. --->
      <cfexit />
    </cfif>
    
    <!--- if we got this far, we can send it to DB table. --->
    <cfset savedToDB = formObj.saveToDatabase() />
    
    <cfif Not savedToDB.success>
      <p class="alert alert-danger">Error saving form submission<cfif savedToDB.msg NEQ "">: #savedToDB.msg#</cfif></p><cfexit />
    </cfif>
    
    <!--- Send notification --->
    <cfset userIsSpam = formObj.sendNotificationEmail(
      to = "mborn@brockettcreative.com",
      from="donotreply@www.example.com"
    ) />
    
  </cfcase>
  <cfdefaultcase>
	  
	<link href="http://cms.tspark.com/tsparkcms/css/formstyle.css" rel="stylesheet">
	<link href="http://cms.tspark.com/tsparkcms/css/tsparkcms.css" rel="stylesheet">
  
    <!--- show the form! --->
    <cfset formObj.renderForm() />
    
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://cms.tspark.com/tsparkcms/js/cms.js"></script>
    
  </cfdefaultcase>
</cfswitch>


<!--- Calculate final result --->
<cfset tickEnd = GetTickCount()>
<cfset testTime = tickEnd - tickBegin>

<!--- Report the results of the test --->
<cfoutput>Test time was: #testTime# milliseconds</cfoutput>