# Coldfusion Form Builder

I got sick of manually typing send-to-email forms with all the associated functionality.

# Features

* build a form by listing the fields and field groups
* includes serverside validation
* includes save-to-database (table column names must match field names)
* includes send-to-email functionality (each field is included in email)
* includes spam trap field

# TODO

* [x] Get it working
* [ ] Write tests
* [ ] Include "validatetype" serverside functionality

